package cz.vsb.ekf.jam.practice.dto;

public enum Sport {
    ICE_HOCKEY, BADMINTON, TENNIS, TABLE_TENNIS, BOX, FOOTBALL, BIATLON;
}
