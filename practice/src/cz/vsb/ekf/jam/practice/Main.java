package cz.vsb.ekf.jam.practice;

import cz.vsb.ekf.jam.practice.dto.Athlete;
import cz.vsb.ekf.jam.practice.dto.Sport;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final SecureRandom random = new SecureRandom();

    public static void main(String[] args) {
        
        List<Athlete> athleteList = new ArrayList<>();  //Puvodni kod inicializace
        //for(int i = 0; i < 5000; i++){
        for(int i = 0; i < 10000; i++){                 //UKOL 7 A)
            Athlete athlete = new Athlete();
            athlete.setId(i);
            athlete.setActive(i % 10 != 0);
            athlete.setBirthDate(LocalDate.now().minusYears(random.nextInt(50)));
            athlete.setName(names[random.nextInt(names.length)]);
            athlete.setSurname("surname-" + i);
            athlete.setLastUpdateTs(LocalDateTime.now().minusDays(random.nextInt(30)));
            athlete.setPrizeMoney(randomBigDecimal(1000000));
            athlete.setSport(randomEnum(Sport.class));
            athleteList.add(athlete);
        }

       appRun(athleteList); //HLAVNI CYKLUS APLIKACE
        
    }

    //PUVODNI KOD - POMOCNE FCE
    public static BigDecimal randomBigDecimal(int range) {
        BigDecimal max = new BigDecimal(range);
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.multiply(max);
        actualRandomDec.setScale(2, RoundingMode.DOWN);
        return actualRandomDec;
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    private static String[] names = {
            "Liam",
            "Olivia",
            "Noah",
            "Emma",
            "Oliver",
            "Ava",
            "William",
            "Sophia",
            "Elijah",
            "Isabella",
            "James",
            "Charlotte",
            "Benjamin",
            "Amelia",
            "Lucas",
            "Mia",
            "Mason",
            "Harper",
            "Ethan",
            "Evelyn"
    };
    
    
    //NOVE POMOCNE FUNKCE
    public static void appRun(List<Athlete> athleteList) //HLAVNI CYKLUS APLIKACE
    {
        showAthletes(athleteList); //2
        top100Athlete(athleteList);//3
        nameRepeatCounter(athleteList);//4
        prizeMoneyOfAll(athleteList);//5
        showTop5ForEverySport(athleteList);//6
        timeCheck(athleteList);//7
        
        
    }
    
    public static void showAthletes(List<Athlete> athleteList) //2
    {
        System.out.println("UKOL 2");
        System.out.println("Jake sportovce chcete zobrazit?\nFor active type 1\nFor inactive type 0 ");
        Scanner myObj = new Scanner(System.in);
        String response = myObj.nextLine();
        
        if(response.equals("1"))
        {
        int countA = 0;
            for(Athlete a : athleteList)
            {
                if(a.getActive())
                    countA++;
            }
        System.out.println("Number of active athletes = " + countA);
        }
        
        else if(response.equals("0"))
        {
            int countN = 0;
            for(Athlete a : athleteList)
                {
                    if(!a.getActive())
                        countN++;
                }
            System.out.println("Number of inactive athletes = " + countN);
        }
        
        else
        System.out.println("Tak nic");
    }
    
    
    public static void top100Athlete(List<Athlete> athleteList) { //3

        System.out.println("UKOL 3");
        System.out.println("Show top100 best athletes?\nFor yes type 1\nFor no type 0");
        Scanner scanner = new Scanner(System.in);
        Scanner myObj = new Scanner(System.in);
        String response = myObj.nextLine();
        
        if (response.equals("1")) {

            List<Athlete> top100 = athleteList;
            Collections.sort(top100,(Athlete a1, Athlete a2) -> (a2.getPrizeMoney().compareTo(a1.getPrizeMoney())));

            for (int i = 0; i < 100; i++)
                    System.out.println("[" + i + "] " + top100.get(i));

        } else if (response.equals("0")) {
            System.out.println("Top100 not shown.");
            
        } else {
            System.out.println("Skipped");
        }

    }
    
    public static void nameRepeatCounter(List<Athlete> athleteList){
        System.out.println("UKOL 4");
        
        System.out.println("Show numbers of same athlete names?\nFor yes type 1\nFor no type 0");
        Scanner myObj = new Scanner(System.in);
        String response = myObj.nextLine();
        
        if(response.equals("1")){
            
            int names2[] = new int[20]; //již vím počet různých jmen z pole names[]
            for(int i = 0; i < 20; i++)
                {
                    names2[i] = 0;
                } //INICIALIZACE JMEN

            for(int i = 0; i < 20; i++)
            {
                for(Athlete a : athleteList)
                {
                    if(a.getName().equals(names[i])) //počítání kolikrát se jednotlivá jména opakují
                       names2[i]++;
                }
            }


            System.out.println("Showing number of names:");
            for(int i = 0; i < 20; i++)
            {
                System.out.println(names[i] + "  \tcount:\t" + names2[i]);
            }
        }
        else if(response.equals("0"))
            System.out.println("Numbers of names not shown.");
        else
            System.out.println("Skipped");
    
    
    }
    
    public static BigDecimal prizeMoneyOfAll(List<Athlete> athleteList)
    {
        System.out.println("UKOL 5");
        
        System.out.println("Show total PrizeMoney?\nFor yes type 1\n For no type 0");
        Scanner myObj = new Scanner(System.in);
        String response = myObj.nextLine();
        
        BigDecimal summm = new BigDecimal("0.00");
        
        if(response.equals("1")){

            for(int i = 0; i < athleteList.size(); i++)
            {
                summm = summm.add(athleteList.get(i).getPrizeMoney());
            }

            for(int i = 0; i < athleteList.size(); i++)
            {
                summm = summm.add(athleteList.get(i).getPrizeMoney());
            }

            System.out.println("SUM of all prizeMoney: " + summm);
            return summm;
        }
        else if(response.equals("0"))
            System.out.println("Total prizemoney not shown.");
        else
            System.out.println("Skipping");
        //KONEC 5  
        return summm;
    }
    
    public static void showTop5ForEverySport(List<Athlete> athleteList)
    {
       System.out.println("UKOL 6");
        
       System.out.println("Show TOP5 for every sport?\nFor yes type 1\n For no type 0");
       Scanner myObj = new Scanner(System.in);
       String response = myObj.nextLine();
        
        
       if(response.equals("1")){
            for(Sport sport : Sport.values())
            {
                System.out.println(sport);
                List<Athlete> top5 = new ArrayList<Athlete>();
                
                for(int i = 0; i < athleteList.size(); i++)
                {
                    if(athleteList.get(i).getSport() == sport)
                        top5.add(athleteList.get(i));
                }
                
                Collections.sort(top5,(Athlete a1, Athlete a2) -> (a1.getBirthDate().compareTo(a2.getBirthDate())));
                
                System.out.println("TOP 5 for sport: " + sport);
                for(int i = 0; i < 5; i++)
                {
                    System.out.println( "[" + i + "] " + top5.get(i));
                }
            }
            
        }
       else if(response.equals("0"))
            System.out.println("Top5 not shown");
       else
            System.out.println("Skipping");
    }
    
    public static void timeCheck(List<Athlete> athleteList)
    {
       System.out.println("UKOL7");
       
       System.out.println("Test run speed?\nFor yes type 1\n For no type 0");
       Scanner myObj = new Scanner(System.in);
       String response = myObj.nextLine();
        
        
       if(response.equals("1")){
        
                    //INIT FOR STANDART ARRAY
            Athlete newAthleteArray[] = new Athlete[10000];
            for(int i = 0; i < 10000; i++)
            {
                Athlete athlete = new Athlete();
                athlete.setId(i);
                athlete.setActive(i % 10 != 0);
                athlete.setBirthDate(LocalDate.now().minusYears(random.nextInt(50)));
                athlete.setName(names[random.nextInt(names.length)]);
                athlete.setSurname("surname-" + i);
                athlete.setLastUpdateTs(LocalDateTime.now().minusDays(random.nextInt(30)));
                athlete.setPrizeMoney(randomBigDecimal(1000000));
                athlete.setSport(randomEnum(Sport.class));
                newAthleteArray[i] = athlete;
            }
            
                    //INIT FOR LINKED LIST
            LinkedList<Athlete> abc = new LinkedList<>();
            for(int i = 0; i < 10000; i++)
            {
                Athlete athlete = new Athlete();
                athlete.setId(i);
                athlete.setActive(i % 10 != 0);
                athlete.setBirthDate(LocalDate.now().minusYears(random.nextInt(50)));
                athlete.setName(names[random.nextInt(names.length)]);
                athlete.setSurname("surname-" + i);
                athlete.setLastUpdateTs(LocalDateTime.now().minusDays(random.nextInt(30)));
                athlete.setPrizeMoney(randomBigDecimal(1000000));
                athlete.setSport(randomEnum(Sport.class));
                abc.add(athlete);
            }
        
        //int a,b,c,d,e,f;
        //int x = LocalDateTime.now().getNano();
        System.out.println("TESTING TIME FOR ARRAYLIST");
        //System.out.println(a = (LocalDateTime.now().getNano() - x));
        
        long start, end;
        start = System.currentTimeMillis();
        System.out.println("Hledani a print sportovce s ID 9999" + athleteList.get(9999));
        end = System.currentTimeMillis();
            System.out.println(start);
            System.out.println(end);
            System.out.println(end - start);
            
        //System.out.println(b =(LocalDateTime.now().getNano() - x));
        //System.out.println("Time it took in nano seconds: " + (b-a));
        
           System.out.println("");
        System.out.println("TESTING TIME FOR LINKED-LIST");
        //System.out.println(e = (LocalDateTime.now().getNano() - x));
        
        start = System.currentTimeMillis();
        System.out.println("Hledani a print sportovce s ID 9999" + abc.get(9999));
        end = System.currentTimeMillis();
            System.out.println(start);
            System.out.println(end);
            System.out.println(end - start);
            
        //System.out.println(f = (LocalDateTime.now().getNano() - x));
        //System.out.println("Time it took in nano seconds: " + (f-e));
        
            System.out.println("");
        
        System.out.println("TESTING TIME FOR STANDART ARRAY");
        //System.out.println(c = (LocalDateTime.now().getNano() - x));
        start = System.currentTimeMillis();
        System.out.println("Searching and print for athlete with ID 9999" + newAthleteArray[9999]);
        end = System.currentTimeMillis();
            System.out.println(start);
            System.out.println(end);
            System.out.println(end - start);
            
        //System.out.println(d = (LocalDateTime.now().getNano() - x));
        //System.out.println("Time it took in nano seconds: " + (d-c));
    }
       else if(response.equals("0"))
            System.out.println("Testing speed skipped");
       else
            System.out.println("Skipped");
    }
    //REGULAR ARRAY -> LINKED LIST -> ARRAYLIST

        
    
    
}
